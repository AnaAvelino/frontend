
## Para executar o projeto, será necessário instalar os seguintes programas:

- [React]
- [Node 14.16.1]

Para prosseguir com o desenvolvimento é necessario clonar o projeto do diretorio a sua escolha

cd "seu diretorio"
git clone https://AnaAvelino@bitbucket.org/AnaAvelino/frontend.git

## Introdução ao Create React App

Este projeto foi iniciado com [Create React App](https://github.com/facebook/create-react-app).

## Executando Comandos
no diretorio do projeto execute os seguintes comandos
 `npm install` instala os modulos listados do package.json
 `npm start`   inicia a aplicação

## Features
O projeto foi desenvolvindo com o intuido de exibir fornecedores de energia, para facilitar a busca feita por eventuais clientes