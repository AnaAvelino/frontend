import React, { Component, useState } from "react";
import "./index.css";
import Login from "./components/Login/login";
import Home from "./pages/Home/home";
import Preferences from "./components/Preferences/Preferences";
import Register from "./components/Login/register";
import { Logout } from "./components/Logout/logout";
import SearchProviders from "./components/SearchProvider/searchProvider";
import useToken from "./components/UseToken/useToken";
import Header from "./components/Header/header";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";

function App() {
  const { token, setToken } = useToken();
  const [restricted, setRestricted] = useState(true);
  console.log(restricted);

  if (!token || token == undefined) {
    return <Login setToken={setToken} />;
  }

  return (
    <Router>
      <div className="App bg-green-500 w-full fixed h-16 ">
        <div className=" flex flex-row-reverse ">
          <div className="mr-6 mt-5 text-white align-bottom">
            <Logout />
          </div>
          <div className="mr-6 mt-5 text-white items-">
            {/* <Link to="/">Home</Link> */}
          </div>
        </div>
      </div>
      <Home />
      <Routes>{/* <Route exact path="/" element={} /> */}</Routes>
    </Router>

    // <div>
    //   <BrowserRouter>
    //     <Routes>
    //       <Route restricted={false} path="/" element={<Home />}></Route>
    //       {/* <Route path="/" element={<Login setToken={setToken} />}></Route> */}

    //       <Route
    //         restricted={false}
    //         path="/login/register"
    //         element={<Register />}
    //       ></Route>
    //     </Routes>
    //   </BrowserRouter>
    // </div>
  );
}

export default App;
