import React, { Component, useState } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import "./style.css";
// import AuthService from "../../services/authService";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";

import PropTypes from "prop-types";
import Register from "./register";
import Rotas from "../../routes";

async function loginUser(credenciais, res) {
  return fetch("http://localhost:8001/api/auth/signin", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ credenciais }),
  })
    .then((data) => data.json())
    .catch((e) => console.log(e));
}

export default function Login({ setToken }) {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const [cadastro, setCadastro] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const token = await loginUser({
      username,
      password,
    });
    setToken(token);
  };
  // function cadastro() {
  //   window.location.href = "/register";
  // }

  return (
    <>
      {cadastro == false ? (
        <div className="login-wrapper bg-green-500 flex items-stretch h-screen">
          <div className="bg-white p-4 justify-items-center my-auto h-96 rounded-xl">
            <div className="h-6"></div>
            <div>
              <div className="text-xl font-semibold">Login</div>
            </div>
            <div className="h-6"></div>
            <form onSubmit={handleSubmit}>
              <div>
                <div>
                  <div>Nome</div>
                </div>
                <div>
                  <input
                    name="username"
                    type="text"
                    onChange={(e) => setUsername(e.target.value)}
                    className="border-2 rounded-md w-96 h-10"
                    required
                  />
                </div>
              </div>
              <div className="h-6"></div>
              <div>
                <div>
                  <div>Senha</div>
                </div>
                <div>
                  <input
                    name="password"
                    type="password"
                    onChange={(e) => setPassword(e.target.value)}
                    className="border-2 rounded-md w-96 h-10"
                    required
                  />
                </div>
              </div>
              <div className="h-6"></div>
              <div className=" self-end">
                <button
                  className=" w-full h-8 bg-black text-xl font-semibold text-white rounded-md hover:bg-slate-800"
                  type="submit"
                >
                  Entrar
                </button>
              </div>
              <div></div>
            </form>
            <div className="h-1"></div>
            <button
              className="w-full h-8 bg-gray-700 text-xl font-semibold text-white rounded-md hover:bg-slate-800"
              onClick={() => setCadastro(true)}
            >
              Cadastre-se
            </button>
          </div>
        </div>
      ) : (
        <Register />
      )}
    </>
  );
}
Login.propTypes = {
  setToken: PropTypes.func.isRequired,
};
