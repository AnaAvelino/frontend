import React, { Component, useState } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";
import PropTypes from "prop-types";
import api from "../../services/api";
import Login from "./login";
export default function Register() {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState(0);
  const [email, setEmail] = useState();
  const [passwordconfirm, setPasswordconfirm] = useState(0);
  const [msg, setmsg] = useState([{ tipo: 0 }]);
  const [cadastro, setLogin] = useState(true);

  async function register(records) {
    return fetch("http://localhost:8001/api/auth/signup", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ records }),
    })
      .then((response) => response.json())
      .then(function (data) {
        setmsg([data]);
      });
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    const token = await register({
      username,
      email,
      password,
      passwordconfirm,
    });
  };
  if (msg[0].tipo == 3) {
    window.location.href = "/";
  }
  function verifyPass(passwordconfirm, password) {
    var arrayMsg = [{ msg: "Atenção: Senha está diferente" }, { tipo: 3 }];
    if (passwordconfirm != password) {
      return setmsg(arrayMsg);
    }
  }
  return (
    <>
      {cadastro == true ? (
        <div className="login-wrapper bg-green-500">
          <div className="login-wrapper flex items-stretch h-screen">
            <div className="bg-white p-4 justify-items-center my-auto rounded-xl">
              <div className="h-6"></div>
              <div>
                <div className="text-xl font-semibold">Cadastro</div>
              </div>
              <div className="h-6"></div>
              <form onSubmit={handleSubmit}>
                <div>
                  <div>
                    <div>Nome</div>
                  </div>
                  <div>
                    <input
                      name="username"
                      type="text"
                      onChange={(e) => setUsername(e.target.value)}
                      className="border-2 rounded-md w-96 h-10"
                      required
                    />
                  </div>
                  <div className="flex justify-end al text-xs text-red-600">
                    <div>{msg[0].tipo == 1 ? msg[0].msg : ""}</div>
                  </div>
                </div>
                <div className="h-6"></div>
                <div>
                  <div>
                    <div>Email</div>
                  </div>
                  <div>
                    <input
                      name="email"
                      type="email"
                      onChange={(e) => setEmail(e.target.value)}
                      className="border-2 rounded-md w-96 h-10"
                      required
                    />
                  </div>
                  <div className="flex justify-end al text-xs text-red-600">
                    <div>{msg[0].tipo == 2 ? msg[0].msg : ""}</div>
                  </div>
                </div>
                <div className="h-6"></div>
                <div>
                  <div>
                    <div>Senha</div>
                  </div>
                  <div>
                    <input
                      name="password"
                      type="password"
                      onChange={(e) => setPassword(e.target.value)}
                      className="border-2 rounded-md w-96 h-10"
                      required
                    />
                  </div>
                </div>
                <div className="h-6"></div>
                <div className="h-6"></div>
                <div className=" self-end">
                  <button
                    className=" w-full h-8 bg-black text-xl font-semibold text-white rounded-md hover:bg-slate-800"
                    type="submit"
                  >
                    Salvar
                  </button>
                  <div className="h-1"></div>
                </div>

                <div></div>
              </form>

              <button
                className="w-full h-8 bg-gray-700 text-xl font-semibold text-white rounded-md hover:bg-slate-800"
                onClick={() => setLogin(false)}
              >
                Entre
              </button>
              <div className="h-1"></div>
            </div>
          </div>
        </div>
      ) : (
        <Login />
      )}
    </>
  );
}
