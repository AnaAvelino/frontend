import React, { Component, useEffect, useState } from "react";
import CardProvider from "../cardProvider/cardProvider";

export default function SearchProviders() {
  const [providers, setProviders] = useState([]);
  const [limit, setLimite] = useState([]);
  console.log("--->", providers);

  async function providersSearch(limit, res) {
    // console.log(limit.limit);
    const dados = fetch(
      `http://localhost:8001/api/auth/providers/${limit.limit}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ limit }),
      }
    )
      .then((response) => response.json())
      .then(function (data) {
        setProviders([...data.providers]);
      });
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    await providersSearch({
      limit,
    });
  };

  return (
    <div className="login-wrapper flex items-stretch h-screen bg-gray-300">
      <div className="bg-white p-4 justify-items-center my-auto h-96 rounded-xl w-2/5">
        <div className="h-6"></div>
        <div>
          <div className="text-xl font-semibold">Buscar Fornecedor</div>
        </div>
        <div className="h-6"></div>
        <form onSubmit={handleSubmit}>
          <div>
            <div>
              <div>kWh</div>
            </div>
            <div>
              <input
                name="limit"
                type="text"
                required
                onChange={(e) => setLimite(e.target.value)}
                className="border-2 rounded-md  h-10 w-full"
              />
            </div>
            {/* <ErrorMessage errors={errors} name="limit" /> */}
          </div>{" "}
          <div className="h-6"></div>
          <div className=" self-end">
            <button
              className=" w-full h-8 bg-black text-xl font-semibold text-white rounded-md hover:bg-slate-800"
              type="submit"
            >
              Pesquisar Fornecedor
            </button>
          </div>
          <div></div>
        </form>
        <div className="h-1">
          <div className="h-2"></div>
          <div className="">
            {Object.entries(providers).length != 0 ? (
              <div className="w-full overflow-y-auto h-40  ">
                <CardProvider providers={providers} />
              </div>
            ) : (
              <div className=" w-full text-center font-semibold">
                Sem Resultados
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
