import React from "react";
import iconEnergy from "../../assets/iconmonstr-light-bulb-15.svg";

export default function cardProvider({ providers = [] }) {
  // console.log("CARDD", providers);
  return (
    <div>
      {providers.map((value, index) => (
        <>
          <div className="h-2"></div>

          <div
            key={index}
            value={value.null}
            className="min-w-max max-w-full grid gap-x-8 gap-y-4 grid-cols-3 py-4 justify-items-center w-full bg-green-500  rounded-xl  text-white font-sans font-semibold"
          >
            <div>
              <div className="h-4"></div>
              <img className="h-20 w-20 " src={iconEnergy} />
            </div>
            <div className="w-full col-span-2">
              <div>Nome: {value.name}</div>
              <div className="h-2 "></div>
              <div>Estado: {value.state}</div>
              <div className="h-2 "></div>
              <div>Custo(kWh): {value.cost_kwh}</div>
              <div className="h-2"></div>
              <div>Limite mínimo (kWh): {value.limit_min_kwh}</div>
              <div className="h-2"></div>
            </div>
          </div>
        </>
      ))}
    </div>
  );
}
