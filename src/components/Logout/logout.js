import React from "react";

export function Logout() {
  function Logout() {
    sessionStorage.clear();
    localStorage.clear();

    window.location.href = "/login";
  }

  return (
    <div>
      <button onClick={(e) => Logout()}>Sair</button>
    </div>
  );
}
