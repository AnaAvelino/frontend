export default function auth() {
  const user = JSON.parse(localStorage.getItem("user"));

  if (user.accessToken) {
    return { Authorization: "Bearer " + user.accessToken };
  } else {
    return { msg: "NÃO TEM ACESSO" };
  }
}
